import 'package:bloc_cubit_practical/core/network/dio_settings.dart';
import 'package:bloc_cubit_practical/data/models/quiz_model.dart';
import 'package:dio/dio.dart';

class QuizzRepositories {
  final DioSettings dio;

  QuizzRepositories({required this.dio});

  Future<QuizModel> getQuiz() async {
    final Response response =
        await dio.dio.get("https://opentdb.com/api.php?amount=1");

    return QuizModel.fromJson(response.data);
  }
}
