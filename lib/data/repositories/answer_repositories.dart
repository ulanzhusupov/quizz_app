import 'package:bloc_cubit_practical/data/models/quiz_model.dart';

class AnswerRepositories {
  List<String> getAnswer(QuizModel model) {
    List<String> answers = [];
    answers.add(model.results?[0].correctAnswer ?? "");
    answers.addAll(model.results?[0].incorrectAnswers ?? []);
    answers.shuffle();
    return answers;
  }
}
