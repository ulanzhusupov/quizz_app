class UserAnswerModel {
  final String question;
  final String userAnswer;
  final String correctAnswer;
  final List<String> allAnswers;

  UserAnswerModel({
    required this.question,
    required this.userAnswer,
    required this.correctAnswer,
    required this.allAnswers,
  });
}
