import 'dart:math';

import 'package:bloc_cubit_practical/presentation/bloc/result_bloc/result_bloc.dart';
import 'package:bloc_cubit_practical/presentation/bloc/result_bloc/result_state.dart';
import 'package:bloc_cubit_practical/presentation/theme/app_fonts.dart';
import 'package:bloc_cubit_practical/resources/resources.dart';
import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class ResultPage extends StatefulWidget {
  const ResultPage({super.key});

  @override
  State<ResultPage> createState() => _ResultPageState();
}

class _ResultPageState extends State<ResultPage> {
  bool confettiPlays = false;
  ConfettiController confettiController =
      ConfettiController(duration: const Duration(seconds: 5));

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    confettiController.play();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    confettiController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        PopScope(
          onPopInvoked: (didPop) => didPop ? false : false,
          child: Scaffold(
            floatingActionButton: SizedBox(
              width: 155,
              height: 30,
              child: FloatingActionButton(
                backgroundColor: Colors.amber,
                onPressed: () {
                  Navigator.of(context).popUntil((route) => route.isFirst);
                },
                child: const Text(
                  "Start again",
                  style: AppFonts.s16W700,
                ),
              ),
            ),
            body: BlocBuilder<ResultBloc, ResultState>(
              builder: (context, state) {
                if (state is ResultLoading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (state is ResultSuccess) {
                  int correctAnswers = state.userAnswers
                      .where((e) => e.userAnswer == e.correctAnswer)
                      .length;
                  return SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        const SizedBox(height: 50),
                        if (correctAnswers < 5)
                          Image.asset(
                            Images.thumbDown,
                            width: 100,
                          ),
                        if (correctAnswers < 7 && correctAnswers >= 5)
                          Image.asset(
                            Images.praise,
                            width: 100,
                          ),
                        if (correctAnswers >= 7)
                          Image.asset(
                            Images.greatJob,
                            width: 100,
                          ),
                        Text(
                          "Your Result: ${state.userAnswers.where((e) => e.userAnswer == e.correctAnswer).length} of ${state.userAnswers.length}",
                          style: AppFonts.s22W700,
                        ),
                        const SizedBox(height: 30),
                        Column(
                          children: state.userAnswers
                              .map((e) => Column(
                                    children: [
                                      HtmlWidget(
                                        e.question,
                                        textStyle: AppFonts.s16W700,
                                      ),
                                      const SizedBox(height: 10),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: e.allAnswers
                                            .map((answer) => Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(vertical: 4.0),
                                                  child: Container(
                                                    padding: const EdgeInsets
                                                        .symmetric(
                                                      vertical: 5,
                                                      horizontal: 20,
                                                    ),
                                                    width: double.infinity,
                                                    decoration: BoxDecoration(
                                                      color:
                                                          determineAnswerColor(
                                                        answer,
                                                        e.userAnswer,
                                                        e.correctAnswer,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8),
                                                      border: Border.all(
                                                        color: const Color
                                                            .fromARGB(
                                                            255, 217, 186, 255),
                                                        width: 1,
                                                      ),
                                                    ),
                                                    child: HtmlWidget(
                                                      e.userAnswer == answer
                                                          ? "$answer (your answer)"
                                                          : answer,
                                                      textStyle:
                                                          AppFonts.s14W700,
                                                    ),
                                                  ),
                                                ))
                                            .toList(),
                                      ),
                                      const SizedBox(height: 20),
                                    ],
                                  ))
                              .toList(),
                        ),
                        const SizedBox(height: 50),
                      ],
                    ),
                  );
                }
                if (state is ResultError) {
                  return Center(
                    child: Text(
                      state.errorText,
                      style: AppFonts.s14W700,
                    ),
                  );
                }
                return const SizedBox();
              },
            ),
          ),
        ),
        ConfettiWidget(
          confettiController: confettiController,
          blastDirection: pi / 2,
          emissionFrequency: 0.1,
          gravity: 0.1,
          colors: const [
            Colors.red,
            Colors.yellow,
          ],
        ),
      ],
    );
  }

  Color determineAnswerColor(
      String answer, String userAnswer, String correctAnswer) {
    if (answer == correctAnswer) {
      return Colors.green;
    } else if (userAnswer == answer && userAnswer != correctAnswer) {
      return Colors.red;
    } else {
      return Colors.transparent;
    }
  }
}
