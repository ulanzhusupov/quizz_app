import 'package:bloc_cubit_practical/presentation/pages/quiz_page.dart';
import 'package:bloc_cubit_practical/presentation/theme/app_fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 53, 1, 117),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              CupertinoIcons.light_max,
              color: Colors.yellow,
              size: 100,
            ),
            const Text(
              "Flutter Quiz App",
              style: AppFonts.s26W700,
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const QuizPage()),
                );
              },
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromARGB(255, 94, 0, 208)),
              child: const Text(
                "Начать Викторину",
                style: AppFonts.s14W400,
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const QuizPage()),
                );
              },
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromARGB(255, 94, 0, 208)),
              child: const Text(
                "Выбрать категорию",
                style: AppFonts.s14W400,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
