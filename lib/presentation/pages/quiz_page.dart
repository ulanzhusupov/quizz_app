import 'package:bloc_cubit_practical/data/models/user_answer_model.dart';
import 'package:bloc_cubit_practical/presentation/bloc/quizz_bloc/quiz_bloc.dart';
import 'package:bloc_cubit_practical/presentation/bloc/quizz_bloc/quiz_event.dart';
import 'package:bloc_cubit_practical/presentation/bloc/quizz_bloc/quiz_state.dart';
import 'package:bloc_cubit_practical/presentation/bloc/result_bloc/result_bloc.dart';
import 'package:bloc_cubit_practical/presentation/bloc/result_bloc/result_event.dart';
import 'package:bloc_cubit_practical/presentation/pages/result_page.dart';
import 'package:bloc_cubit_practical/presentation/theme/app_fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class QuizPage extends StatefulWidget {
  const QuizPage({super.key});

  @override
  State<QuizPage> createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  List<UserAnswerModel> userAnswersList = [];
  String userAnswer = "";
  int questionCount = 0;

  @override
  void initState() {
    super.initState();
    questionCount++;
    BlocProvider.of<QuizBloc>(context).add(GetQuizEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<QuizBloc, QuizState>(
        builder: (context, state) {
          if (state is QuizLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is QuizSuccess) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(height: 100),
                  Center(
                    child: HtmlWidget(
                      state.model.results?[0].question ?? "Empty",
                      textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Text(
                    "Correct answer: ${state.model.results?[0].correctAnswer}",
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.5,
                    child: ListView(
                      children: state.answers
                          .map(
                            (item) => Padding(
                              padding: const EdgeInsets.symmetric(vertical: 4),
                              child: InkWell(
                                onTap: () {
                                  if (userAnswer == "") {
                                    userAnswer = item;
                                    userAnswersList.add(UserAnswerModel(
                                      question:
                                          state.model.results?[0].question ??
                                              "",
                                      userAnswer: userAnswer,
                                      correctAnswer: state.model.results?[0]
                                              .correctAnswer ??
                                          "",
                                      allAnswers: state.answers,
                                    ));
                                    BlocProvider.of<ResultBloc>(context).add(
                                        GetResultEvent(
                                            userAnswers: userAnswersList));
                                    setState(() {});
                                  }
                                },
                                child: AnimatedContainer(
                                  duration: const Duration(milliseconds: 500),
                                  color: (userAnswer == item &&
                                          userAnswer ==
                                              state.model.results?[0]
                                                  .correctAnswer)
                                      ? Colors.green
                                      : (userAnswer == item &&
                                              userAnswer !=
                                                  state.model.results?[0]
                                                      .correctAnswer)
                                          ? Colors.red
                                          : Colors.amber,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: HtmlWidget(
                                      item,
                                      textStyle: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                  SizedBox(
                    width: 250,
                    height: 50,
                    child: questionCount == 10
                        ? ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => const ResultPage(),
                                  ));
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  const Color.fromARGB(255, 104, 0, 231),
                              foregroundColor:
                                  const Color.fromARGB(255, 229, 210, 253),
                            ),
                            child: const Text(
                              "Show Results",
                              style: AppFonts.s16W700,
                            ),
                          )
                        : ElevatedButton(
                            onPressed: () {
                              questionCount++;
                              userAnswer = "";
                              // BlocProvider.of<ResultBloc>(context).add(
                              //     GetResultEvent(userAnswers: userAnswersList));
                              BlocProvider.of<QuizBloc>(context)
                                  .add(GetQuizEvent());
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  const Color.fromARGB(255, 104, 0, 231),
                              foregroundColor:
                                  const Color.fromARGB(255, 229, 210, 253),
                            ),
                            child: const Text(
                              "Next question",
                              style: AppFonts.s16W700,
                            ),
                          ),
                  ),
                ],
              ),
            );
          }
          if (state is QuizError) {
            return Center(
              child: Text(
                state.errorText,
                style: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Colors.white,
                ),
              ),
            );
          }
          return const SizedBox();
        },
      ),
    );
  }
}
