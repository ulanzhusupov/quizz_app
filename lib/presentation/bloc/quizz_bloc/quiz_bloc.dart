import 'package:bloc/bloc.dart';
import 'package:bloc_cubit_practical/data/models/quiz_model.dart';
import 'package:bloc_cubit_practical/data/repositories/answer_repositories.dart';
import 'package:bloc_cubit_practical/data/repositories/quiz_repositories.dart';
import 'package:bloc_cubit_practical/presentation/bloc/quizz_bloc/quiz_event.dart';
import 'package:bloc_cubit_practical/presentation/bloc/quizz_bloc/quiz_state.dart';

class QuizBloc extends Bloc<QuizEvent, QuizState> {
  final QuizzRepositories quizzRepositories;

  QuizBloc({required this.quizzRepositories}) : super(QuizInitial()) {
    on<GetQuizEvent>((event, emit) async {
      emit(QuizLoading());
      try {
        QuizModel quizModel = await quizzRepositories.getQuiz();
        List<String> answers = AnswerRepositories().getAnswer(quizModel);

        emit(
          QuizSuccess(
            model: quizModel,
            answers: answers,
          ),
        );
      } catch (e) {
        emit(QuizError(errorText: e.toString()));
      }
    });
  }
}
