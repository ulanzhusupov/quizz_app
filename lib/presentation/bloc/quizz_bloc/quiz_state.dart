import 'package:bloc_cubit_practical/data/models/quiz_model.dart';

sealed class QuizState {}

class QuizInitial extends QuizState {}

class QuizLoading extends QuizState {}

class QuizSuccess extends QuizState {
  final QuizModel model;
  final List<String> answers;

  QuizSuccess({
    required this.model,
    required this.answers,
  });
}

class QuizError extends QuizState {
  final String errorText;

  QuizError({required this.errorText});
}
