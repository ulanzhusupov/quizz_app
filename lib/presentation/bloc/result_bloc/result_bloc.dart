import 'package:bloc_cubit_practical/presentation/bloc/result_bloc/result_event.dart';
import 'package:bloc_cubit_practical/presentation/bloc/result_bloc/result_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ResultBloc extends Bloc<ResultEvent, ResultState> {
  ResultBloc() : super(ResultInitial()) {
    on<GetResultEvent>((event, emit) {
      emit(ResultLoading());
      print("Ответы поль-я: ${event.userAnswers.toList()}");
      try {
        emit(ResultSuccess(userAnswers: event.userAnswers));
      } catch (e) {
        emit(ResultError(errorText: e.toString()));
      }
    });
  }
}
