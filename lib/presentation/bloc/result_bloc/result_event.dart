import 'package:bloc_cubit_practical/data/models/user_answer_model.dart';

abstract class ResultEvent {}

// class GetResultEvent extends ResultEvent {}

class GetResultEvent extends ResultEvent {
  final List<UserAnswerModel> userAnswers;

  GetResultEvent({required this.userAnswers});
}
