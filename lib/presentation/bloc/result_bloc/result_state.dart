import 'package:bloc_cubit_practical/data/models/user_answer_model.dart';

sealed class ResultState {}

class ResultInitial extends ResultState {}

class ResultLoading extends ResultState {}

class ResultSuccess extends ResultState {
  final List<UserAnswerModel> userAnswers;

  ResultSuccess({
    required this.userAnswers,
  });
}

class ResultError extends ResultState {
  final String errorText;

  ResultError({required this.errorText});
}
