abstract class ScoreEvent {}

class QuizzScoreEvent extends ScoreEvent {
  final int correctAnswers;
  final int questions;

  QuizzScoreEvent({
    required this.correctAnswers,
    required this.questions,
  });
}
