import 'package:bloc_cubit_practical/presentation/bloc/score_bloc/score_event.dart';
import 'package:bloc_cubit_practical/presentation/bloc/score_bloc/score_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ScoreBloc extends Bloc<ScoreEvent, ScoreState> {
  ScoreBloc() : super(ScoreInitial()) {
    on<QuizzScoreEvent>(
      (event, emit) {
        try {
          // event.answer ==
          emit(ScoreSuccess(
              correctAnswers: event.correctAnswers + 1,
              questions: event.questions + 1));
        } catch (e) {
          emit(ScoreError(errorText: e.toString()));
        }
      },
    );
  }
}
