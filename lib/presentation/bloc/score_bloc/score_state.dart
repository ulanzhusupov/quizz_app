sealed class ScoreState {}

class ScoreInitial extends ScoreState {}

class ScoreSuccess extends ScoreState {
  final int correctAnswers;
  final int questions;

  ScoreSuccess({
    required this.correctAnswers,
    required this.questions,
  });
}

class ScoreError extends ScoreState {
  final String errorText;

  ScoreError({required this.errorText});
}
