part of 'resources.dart';

class Images {
  Images._();

  static const String congrats = 'assets/png/congrats.png';
  static const String greatJob = 'assets/png/great_job.png';
  static const String praise = 'assets/png/praise.png';
  static const String thumbDown = 'assets/png/thumb_down.png';
}
