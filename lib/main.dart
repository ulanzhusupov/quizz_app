import 'package:bloc_cubit_practical/core/network/dio_settings.dart';
import 'package:bloc_cubit_practical/data/repositories/quiz_repositories.dart';
import 'package:bloc_cubit_practical/presentation/bloc/quizz_bloc/quiz_bloc.dart';
import 'package:bloc_cubit_practical/presentation/bloc/result_bloc/result_bloc.dart';
import 'package:bloc_cubit_practical/presentation/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => DioSettings(),
        ),
        RepositoryProvider(
          create: (context) => QuizzRepositories(
              dio: RepositoryProvider.of<DioSettings>(context)),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) => QuizBloc(
                  quizzRepositories:
                      RepositoryProvider.of<QuizzRepositories>(context))),
          BlocProvider(create: (context) => ResultBloc()),
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(
              seedColor: Colors.deepPurple,
              background: const Color.fromARGB(255, 53, 1, 117),
            ),
            fontFamily: GoogleFonts.mulish().fontFamily,
            useMaterial3: true,
          ),
          debugShowCheckedModeBanner: false,
          home: const HomePage(),
        ),
      ),
    );
  }
}
