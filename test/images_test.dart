import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_cubit_practical/resources/resources.dart';

void main() {
  test('images assets test', () {
    expect(File(Images.congrats).existsSync(), isTrue);
    expect(File(Images.greatJob).existsSync(), isTrue);
    expect(File(Images.praise).existsSync(), isTrue);
    expect(File(Images.thumbDown).existsSync(), isTrue);
  });
}
